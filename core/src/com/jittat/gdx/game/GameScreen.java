package com.jittat.gdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.ScreenAdapter;

public class GameScreen extends ScreenAdapter {
	
	private PacmanGame pacmanGame;
	private World world;
	private WorldRenderer worldRenderer;

	public GameScreen(PacmanGame pacmanGame) {
		this.pacmanGame = pacmanGame;
		
		world = new World();
		worldRenderer = new WorldRenderer(pacmanGame.batch, world);
	}

	@Override
	public void render(float delta) {
		update(delta);
		worldRenderer.render();
	}

	private void update(float delta) {
		updateKeyPressed();
		world.update(delta);
	}

	private void updateKeyPressed() {
		boolean isKeyPressed = false;
		Pacman pacman = world.getPacman();
		
		if(Gdx.input.isKeyPressed(Keys.LEFT)) {
			pacman.setNextDirection(Pacman.Direction.LEFT);
			isKeyPressed = true;
		}
		if(Gdx.input.isKeyPressed(Keys.RIGHT)) {
			pacman.setNextDirection(Pacman.Direction.RIGHT);
			isKeyPressed = true;
		}
		if(Gdx.input.isKeyPressed(Keys.UP)) {
			pacman.setNextDirection(Pacman.Direction.UP);
			isKeyPressed = true;
		}
		if(Gdx.input.isKeyPressed(Keys.DOWN)) {
			pacman.setNextDirection(Pacman.Direction.DOWN);
			isKeyPressed = true;
		}
		
		if(!isKeyPressed) {
			pacman.setNextDirection(Pacman.Direction.STILL);
		}
	}
}
