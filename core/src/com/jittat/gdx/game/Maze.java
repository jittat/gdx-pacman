package com.jittat.gdx.game;

public class Maze {
	private int height;
	private int width;
	private boolean [][] dots;
	
	private String[] MAP = new String [] {
			"####################",
            "#..................#",
            "#.###.###..###.###.#",
            "#.#...#......#...#.#",
            "#.#.###.####.###.#.#",
            "#.#.#..........#.#.#",
            "#.....###..###.....#",
            "#.#.#..........#.#.#",
            "#.#.###.####.###.#.#",
            "#.#...#......#...#.#",
            "#.###.###..###.###.#",
            "#..................#",
            "####################"	
    };
	
	public Maze() {
		this.setHeight(MAP.length);
		this.setWidth(MAP[0].length());
		
		buildDotMap();
	}

	private void buildDotMap() {
		dots = new boolean[height][];
		for(int r = 0; r < height; r++) {
			dots[r] = new boolean[width];
			for(int c = 0; c < width; c++) {
				dots[r][c] = (MAP[r].charAt(c) == '.');
			}
		}
	}

	private char getMapChar(int r, int c) {
		return MAP[r].charAt(c);
	}
	
	public boolean hasWallAt(int r, int c) {
		return getMapChar(r,c) == '#';
	}
	
	public boolean hasDotAt(int r, int c) {
		return dots[r][c];
	}

	public void removeDotAt(int r, int c) {
		dots[r][c] = false;
	}
	
	public int getHeight() {
		return height;
	}

	private void setHeight(int height) {
		this.height = height;
	}

	public int getWidth() {
		return width;
	}

	private void setWidth(int width) {
		this.width = width;
	} 
}
