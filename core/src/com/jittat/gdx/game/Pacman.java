package com.jittat.gdx.game;

import com.badlogic.gdx.math.Vector2;

public class Pacman {	
	public enum Direction {
		UP(1), RIGHT(2), DOWN(3), LEFT(4), STILL(0);
		
		private int val;

		Direction(int val) {
			this.val = val;
		}
		public int getVal() {
			return val;
		}
	}
	
	static final int SPEED = 4;
	private Maze maze;
	private Vector2 position;
	private int mazeC;
	private int mazeR;
	private int blockSize;

	static final int[][] movementOffsets = new int[][] {
		{0,0},
		{0,-1},
		{1,0},
		{0,1},
		{-1,0}
	};
	
	Direction currentDirection;
	Direction nextDirection;
	
	public Pacman(int x, int y, int blockSize, Maze maze) {
		position = new Vector2(x,y);
		this.blockSize = blockSize;
		this.maze = maze;
		
		currentDirection = Direction.STILL;
		nextDirection = Direction.STILL;
		
		updateMazePosition();
	}

	private boolean isAtCenter() {
		int xOffset = getX() % blockSize;
		int yOffset = getY() % blockSize;
		
		return (xOffset == blockSize / 2) && (yOffset == blockSize / 2);
	}
	
	public void setNextDirection(Direction dir) {
		nextDirection = dir;
	}
	
	public void update() {
		if(isAtCenter()) {
			updateMazePosition();			
			eatDot();
			
			currentDirection = nextDirection;
			if(!checkMovement(currentDirection)) {
				currentDirection = Direction.STILL;
			}
		}
		position.x += SPEED * movementOffsets[currentDirection.getVal()][0];
		position.y += SPEED * movementOffsets[currentDirection.getVal()][1];
	}
	
	private void updateMazePosition() {
		mazeR = (int)(position.y - blockSize/2) / blockSize;
		mazeC = (int)(position.x - blockSize/2) / blockSize;		
	}
	
	private void eatDot() {
		if(maze.hasDotAt(mazeR, mazeC)) {
			maze.removeDotAt(mazeR, mazeC);
		}
	}

	private boolean checkMovement(Direction dir) {
		int nr = mazeR + movementOffsets[dir.getVal()][1];
		int nc = mazeC + movementOffsets[dir.getVal()][0];
		
		if((nr < 0) || (nr >= maze.getHeight()) ||
			(nc < 0) || (nc >= maze.getWidth())) {
			return false;
		}
		if(maze.hasWallAt(nr, nc)) {
			return false;
		}
		return true;
	}

	public int getX() {
		return (int) position.x;
	}

	public int getY() {
		return (int) position.y;
	}
}
