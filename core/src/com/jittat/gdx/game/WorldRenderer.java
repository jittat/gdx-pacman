package com.jittat.gdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class WorldRenderer {
	private MazeRenderer mazeRenderer;
	private World world;
	private SpriteBatch batch;
	
	private Texture pacmanImage;
	
	public WorldRenderer(SpriteBatch batch, World world) {
		this.world = world;
		this.batch = batch;
	
		pacmanImage = new Texture("pacman.png");
		
		mazeRenderer = new MazeRenderer(batch, world.getMaze());
	}
	
	public void render() {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		mazeRenderer.render();
		
		Pacman pacman = world.getPacman();
		
		batch.begin();
		batch.draw(pacmanImage, 
				pacman.getX() - 20, 
				PacmanGame.HEIGHT - pacman.getY() - 20);
		batch.end();
	}
}
