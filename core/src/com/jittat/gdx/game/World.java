package com.jittat.gdx.game;

public class World {
	private Maze maze;
	private Pacman pacman;

	public World() {
		maze = new Maze();
		pacman = new Pacman(60, 60, 40, maze);
	}

	public Maze getMaze() {
		return maze;
	}

	public Pacman getPacman() {
		return pacman;
	}

	public void update(float delta) {
		pacman.update();
	}
}
